const width = 1200;
const height = 800;

var targetPos = new p5.Vector(width - 50, height / 2);
var targetRadius = 40;

const sourcePos = new p5.Vector(50, height / 2);
const sourceRadius = 40;

var settings = {
  numberOfRockets: 600,
  rocketLifespan: 200,
  magnitude: 0.25,
  mutationRate: 0.01,
  successScore: (score) => { return score * 8; },
  crashScore: (score) => { return 0; },
  bonusScore: (score, bonusScore) => { return score + bonusScore; }
}

var rocket;
var population;
var successRate = 0;

var generation = 0;
var rocketAge = 0;
var currentMaxFit = 0;
var currentSuccess = 0;

var obstacles = [];
var powerAreas = [];
var matingPool = [];

// Obstacle
class Obstacle {
  constructor(position) {
    this.position = position;
    this.height = 0;
    this.width = 0;
  }

  show() {
    fill(120, 0, 0);
    noStroke();
    rect(this.position.x, this.position.y, this.width, this.height);
  }
}

// Rocket DNA
class DNA {
  constructor(genes) {
    if (genes) {
      this.genes = genes;
    } else {
      this.genes = [];

      for (var i = 0; i < settings.rocketLifespan; i++) {
        this.genes[i] = p5.Vector.random2D();
        this.genes[i].setMag(settings.magnitude);
      }
    }
  }

  crossover(partner) {
    const newGenes = [];
    const mid = floor(random(this.genes.length));
    for (var i = 0; i < this.genes.length; i++) {
      if (i > mid) {
        newGenes[i] = this.genes[i];
      } else {
        newGenes[i] = partner.genes[i];
      }
      newGenes[i] = this.mutate(newGenes[i]);
      newGenes[i].setMag(settings.magnitude);
    }

    return new DNA(newGenes);
  }

  mutate(original) {
    var changed = original;
    if (random(1) < settings.mutationRate) {
      changed = p5.Vector.random2D();
    }
    return changed;
  }
}

class PowerArea {
  constructor(position) {
    this.position = position;
    this.size = 40;
  }

  show() {
    noStroke();
    fill(0, 120, 0, 175);
    ellipse(this.position.x, this.position.y, this.size, this.size);
  }
}

// Rocket class definition
class Rocket {
  constructor(dna) {
    this.position = sourcePos.copy();
    this.velocity = createVector(0, 0);
    this.acceleration = createVector();
    this.height = 20;
    this.width = 3;
    this.fitness = 0;
    this.powerUps = 0;
    this.completed = false;
    this.crashed = false;

    if (dna) {
      this.dna = dna;
    } else {
      this.dna = new DNA();
    }
  }

  update() {
    // Calculate the distance to target
    const distance = dist(this.position.x, this.position.y, targetPos.x, targetPos.y);
    if (distance < targetRadius / 2) {
      this.completed = true;
      this.position = targetPos.copy();
    }

    // Check for off screen
    if (this.position.x > width || this.position.x < 0) {
      this.crashed = true;
    }
    if (this.position.y > height || this.position.y < 0) {
      this.crashed = true;
    }

    // TODO: Change to foreach loop
    // Check for collissions
    for (var i = 0; i < obstacles.length; i++) {
      const obs = obstacles[i];
      if (
        this.position.x > obs.position.x
        && this.position.x < obs.position.x + obs.width
        && this.position.y > obs.position.y
        && this.position.y < obs.position.y + obs.height) {
          this.crashed = true;
          break;
        }
    }

    for (var i = 0; i < powerAreas.length; i++) {
      const powerDistance = dist(this.position.x, this.position.y,
        powerAreas[i].position.x, powerAreas[i].position.y);
      if (powerDistance < powerAreas[i].size / 2) {
        this.powerUps += 10;
      }
    }

    this.applyForce(this.dna.genes[rocketAge]);

    if (!this.completed && !this.crashed) {
      this.velocity.add(this.acceleration);
      this.position.add(this.velocity);
      this.acceleration.mult(0);
    }
  }

  applyForce(force) {
    this.acceleration.add(force);
  }

  calculateFitness() {
    if (this.crashed) {
      this.fitness = settings.crashScore(this.fitness);
      return;
    }

    const distance = dist(this.position.x, this.position.y, targetPos.x, targetPos.y);
    this.fitness = floor(map(distance, 0, width, width, 0));

    if (this.completed) {
      this.fitness = settings.successScore(this.fitness);
    }
    if (this.powerUps) {
      this.fitness = settings.bonusScore(this.fitness, this.powerUps)
    }
  }

  show() {
    if (this.completed || this.crashed) return;

    push();
    noStroke();
    fill(255);
    translate(this.position.x, this.position.y);
    rotate(this.velocity.heading());

    ellipse(0, 0, 4, 4);

    // rectMode(CENTER);
    // rect(0, 0, this.height, this.width);

    // fill(255, 0, 0);
    // triangle(this.height / 2, this.width * -1.25, (this.height / 2) + this.width * 1.5, 0, this.height / 2, this.width * 1.25);
    pop();
  }
}

class Population {
  constructor(source) {
    this.rockets = [];
    this.matingPool = [];
    this.population = settings.numberOfRockets;
    for (var i = 0; i < this.population; i++) {
      this.rockets[i] = new Rocket();
      this.rockets[i].velocity = p5.Vector.random2D();
    }
  }

  run() {
    for (var i = 0; i < this.rockets.length; i++) {
      this.rockets[i].update();
      this.rockets[i].show();
    }
  }

  evaluate() {
    currentMaxFit = 0;
    currentSuccess = 0;
    for (var i = 0; i < this.rockets.length; i++) {
      if (this.rockets[i].completed) currentSuccess++;
      this.rockets[i].calculateFitness();
      if (this.rockets[i].fitness > currentMaxFit) {
        currentMaxFit = this.rockets[i].fitness;
      }
    }

    for (var i = 0; i < this.rockets.length; i++) {
      this.rockets[i].fitness /= currentMaxFit; // Divide by zero issue? :s
    }

    this.matingPool = [];
    for (var i = 0; i < this.rockets.length; i++) {
      const n = this.rockets[i].fitness * 100;
      for (var j = 0; j < n; j++) {
        this.matingPool.push(this.rockets[i]);
      }
    }
  }

  selection() {
    if (this.matingPool.length == 0) {
      console.log("Game over.", this.matingPool.length);
      return false;
    }

    // TODO: Remove parent after picking it!
    const newRockets = [];

    for (var i = 0; i < this.rockets.length; i++) {
      const parentA = random(this.matingPool).dna;
      const parentB = random(this.matingPool).dna;

      const child = parentA.crossover(parentB);
      newRockets[i] = new Rocket(child);
    }

    this.rockets = newRockets;
    return true;
  }
}

var angle;

function setup() {
  angle = 0;
  createCanvas(width, height);
  population = new Population();

  let powerArea1 = new PowerArea(createVector((width / 3) * 1, (height / 3) * 0.75));
  powerArea1.size = 100;
  powerAreas.push(powerArea1);

  let powerArea2 = new PowerArea(createVector((width / 3) * 2, (height / 3) * 1.25));
  powerArea2.size = 50;
  powerAreas.push(powerArea2);

  let obstacle1 = new Obstacle(createVector((width / 3) * 0.75, (height / 3) * 0));
  obstacle1.width = 15;
  obstacle1.height = 150;
  obstacles.push(obstacle1);

  let obstacle2 = new Obstacle(createVector((width / 3) * 0.75, (height / 3) * 2));
  obstacle2.width = 15;
  obstacle2.height = 150;
  obstacles.push(obstacle2);

  let obstacle3 = new Obstacle(createVector((width / 3) * 1.40, (height / 3) * 1));
  obstacle3.width = 15;
  obstacle3.height = 150;
  obstacles.push(obstacle3);

  let obstacle4 = new Obstacle(createVector((width / 3) * 2, (height / 3) * 0));
  obstacle4.width = 15;
  obstacle4.height = 150;
  obstacles.push(obstacle4);

  let obstacle5 = new Obstacle(createVector((width / 3) * 2, (height / 3) * 2));
  obstacle5.width = 15;
  obstacle5.height = 150;
  obstacles.push(obstacle5);


  //frameRate(10);
  background(0);
}

var gameOver = false;

function draw() {
  const currentFps = frameRate();
  // Draw background
  background('rgba(0,0,0, 0.3)');

  if (gameOver) {
    fill(120);
    textSize(72);
    textAlign(CENTER);
    text("GAME OVER!", width/2, height/2);
    return;
  }

  for (var i = 0; i < this.powerAreas.length; i++) {
    this.powerAreas[i].show();
  }

  // Draw rocket
  fill(120, 120, 0);

  population.run();
  if (rocketAge == settings.rocketLifespan) {
    population.evaluate();
    gameOver = !population.selection();
    rocketAge = 0;
    generation++;
    successRate = (currentSuccess/settings.numberOfRockets);
    console.log(`Generation ${generation}, success: ${floor(successRate*100)} %`);
  }

  // Draw obstacles
  for (var i = 0; i < obstacles.length; i++) {
    obstacles[i].show();
  }

  // Draw some text
  fill(120);
  textSize(16);
  textAlign(RIGHT);

  var textYPos = 30;
  const fontSize = 12;
  const textMargin = 4;

  // Draw the rockets' age
  // text(`Age: ${rocketAge}`, width-30, textYPos);
  // textYPos += fontSize + textMargin;

  // Draw the current max fit
  text(`Max score: ${floor(currentMaxFit)}`, width-30, textYPos);
  textYPos += fontSize + textMargin;

  // Draw the current generation
  text(`Generation: ${floor(generation)}`, width-30, textYPos);
  textYPos += fontSize + textMargin;

  // Draw success
  text(`Success rate: ${floor(successRate*100)} %`, width-30, textYPos);
  textYPos += fontSize + textMargin;

  // Draw FPS
  text(`FPS: ${floor(currentFps)}`, width-30, textYPos);
  textYPos += fontSize + textMargin;

  const targetOuterRadius = targetRadius + (sin(angle) * targetRadius / 1.5) + targetRadius * 3;
  //const opacity = (sin(angle) * 150/3) + (150/3) + 25;
  const opacity = map(sin(angle), -1, 1, 50, 10);
  angle += 0.035;

  // Draw target
  fill(128, 0, 0);
  noStroke();
  ellipse(targetPos.x, targetPos.y, targetRadius, targetRadius);

  // Draw outer area of target
  fill(128, 0, 0, opacity);
  stroke(128, 0, 0, 200);
  ellipse(targetPos.x, targetPos.y, targetOuterRadius, targetOuterRadius);

  // Draw source
  fill(0, 128, 0);
  noStroke();
  ellipse(sourcePos.x, sourcePos.y, sourceRadius, sourceRadius);

  // Draw outer area of source
  fill(0, 128, 0, opacity);
  stroke(0, 128, 0, 200);
  ellipse(sourcePos.x, sourcePos.y, targetOuterRadius, targetOuterRadius);

  stroke(120, 200);
  noFill();
  ellipse(width-30, height-30, 50, 50);

  fill(120, 120, 0, 200);
  noStroke();
  ellipse(width-30, height-30, (rocketAge / settings.rocketLifespan) * 50, (rocketAge / settings.rocketLifespan) * 50);

  rocketAge++;
}

function mouseClicked() {
  console.log(`mouseClicked: ${mouseX}, ${mouseY}`);
  if (mouseX > 0 && mouseX < width && mouseY > 0 && mouseY < height) {
    targetPos = createVector(mouseX, mouseY);
  }
  // prevent default
  return false;
}
